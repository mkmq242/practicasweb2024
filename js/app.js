const selMarc = document.getElementById('selmarc');
const secciones = document.querySelectorAll('.contenedorFlex');
titulos = {
    Lamborgini: document.getElementById('tituloLamborghini'),
    Honda: document.getElementById('tituloHonda'),
    Ford: document.getElementById('tituloFord')
};


selMarc.addEventListener('change', function cambio() {
    const opcion = selMarc.value;
    secciones.forEach(seccion => {
        if (seccion.id === opcion) {
            seccion.style.display = 'flex';
            titulos[opcion].style.display = 'block';
        } else {
            seccion.style.display = 'none';
            titulos[seccion.id].style.display = 'none';
        }
    });
});
